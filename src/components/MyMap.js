import React, { useState } from 'react';
import '../styles/MyMap.css';
import { Map, TileLayer, ImageOverlay, Circle } from 'react-leaflet';
import HeatmapLayer from 'react-leaflet-heatmap-layer';
import axios from 'axios';

let capitais = [
  { CAPITAL: 'RIO BRANCO', COORDENADAS: [-8.77, -70.55] },
  { CAPITAL: 'MACEIO', COORDENADAS: [-9.71, -35.73] },
  { CAPITAL: 'MANAUS', COORDENADAS: [-3.07, -61.66] },
  { CAPITAL: 'MACAPA', COORDENADAS: [1.41, -51.77] },
  { CAPITAL: 'SALVADOR', COORDENADAS: [-12.96, -38.51] },
  { CAPITAL: 'FORTALEZA', COORDENADAS: [-3.7327, -38.527] },
  { CAPITAL: 'BRASILIA', COORDENADAS: [-15.8267, -47.9218] },
  { CAPITAL: 'VITORIA', COORDENADAS: [-19.19, -40.34] },
  { CAPITAL: 'GOIANIA', COORDENADAS: [-16.6869, -49.2648] },
  { CAPITAL: 'SAO LUIS', COORDENADAS: [-2.55, -44.3] },
  { CAPITAL: 'CUIABA', COORDENADAS: [-15.6009, -56.0968] },
  { CAPITAL: 'CAMPO GRANDE', COORDENADAS: [-20.4697, -54.6201] },
  { CAPITAL: 'BELO HORIZONTE', COORDENADAS: [-19.9167, -43.9345] },
  { CAPITAL: 'BELEM', COORDENADAS: [-1.4558, -48.4902] },
  { CAPITAL: 'JOAO PESSOA', COORDENADAS: [-7.06, -35.55] },
  { CAPITAL: 'CURITIBA', COORDENADAS: [-25.4809, -49.3044] },
  { CAPITAL: 'RECIFE', COORDENADAS: [-8.28, -35.07] },
  { CAPITAL: 'TERESINA', COORDENADAS: [-8.28, -43.68] },
  { CAPITAL: 'RIO DE JANEIRO', COORDENADAS: [-22.84, -43.15] },
  { CAPITAL: 'NATAL', COORDENADAS: [-5.22, -36.52] },
  { CAPITAL: 'PORTO VELHO', COORDENADAS: [-11.22, -62.8] },
  { CAPITAL: 'PORTO ALEGRE', COORDENADAS: [-30.01, -51.22] },
  { CAPITAL: 'BOA VISTA', COORDENADAS: [2.81954, -60.6714] },
  { CAPITAL: 'FLORIANOPOLIS', COORDENADAS: [-27.5949, -48.5482] },
  { CAPITAL: 'ARACAJU', COORDENADAS: [-10.9472, -37.0731] },
  { CAPITAL: 'SAO PAULO', COORDENADAS: [-23.55, -46.64] },
  { CAPITAL: 'PALMAS', COORDENADAS: [-10.25, -48.25] },
];

let estados = [
  { state: 'AC', coords: [-8.77, -70.55] },
  { state: 'AL', coords: [-9.62, -36.82] },
  { state: 'AM', coords: [-3.47, -65.1] },
  { state: 'AP', coords: [1.41, -51.77] },
  { state: 'BA', coords: [-13.29, -41.71] },
  { state: 'CE', coords: [-5.2, -39.53] },
  { state: 'DF', coords: [-15.83, -47.86] },
  { state: 'ES', coords: [-19.19, -40.34] },
  { state: 'GO', coords: [-15.98, -49.86] },
  { state: 'MA', coords: [-5.42, -45.44] },
  { state: 'MT', coords: [-12.64, -55.42] },
  { state: 'MS', coords: [-20.51, -54.54] },
  { state: 'MG', coords: [-18.1, -44.38] },
  { state: 'PA', coords: [-3.79, -52.48] },
  { state: 'PB', coords: [-7.28, -36.72] },
  { state: 'PR', coords: [-24.89, -51.55] },
  { state: 'PE', coords: [-8.38, -37.86] },
  { state: 'PI', coords: [-6.6, -42.28] },
  { state: 'RJ', coords: [-22.25, -42.66] },
  { state: 'RN', coords: [-5.81, -36.59] },
  { state: 'RO', coords: [-10.83, -63.34] },
  { state: 'RS', coords: [-30.17, -53.5] },
  { state: 'RR', coords: [1.99, -61.33] },
  { state: 'SC', coords: [-27.45, -50.95] },
  { state: 'SE', coords: [-10.57, -37.45] },
  { state: 'SP', coords: [-22.19, -48.79] },
  { state: 'TO', coords: [-9.46, -48.26] },
];

function getHeatPoints() {
  let heatPoints = [];

  axios
    .get('https://apitempo.inmet.gov.br/condicao/capitais/2019-10-22')
    .then((response) => {
      response.data.forEach((object) => {
        capitais.forEach((capital) => {
          if (
            object.CAPITAL.toUpperCase()
              .normalize('NFD')
              .replace(/[\u0300-\u036f]/g, '') ===
            capital.CAPITAL.toUpperCase()
              .normalize('NFD')
              .replace(/[\u0300-\u036f]/g, '')
          ) {
            object.COORDENADAS = capital.COORDENADAS;
            heatPoints.push([
              object.COORDENADAS[0],
              object.COORDENADAS[1],
              object.TMAX18.replace('*', '') / 42,
            ]);
          }
        });
      });
    });

  return heatPoints;
}

class MyMap extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      casos: [],
    };
  }

  componentDidMount() {
    getHeatPoints();
    this.getCovid();
  }

  getCovid() {
    axios
      .get('https://covid19-brazil-api.now.sh/api/report/v1/brazil/20200318')
      .then((response) => {
        this.setState({ casos: response.data.data });
      });
  }
  renderCovid() {
    console.log(this.state.casos);
    return this.state.casos.map((object) => {
      return estados.map((estado) => {
        if (object.uf === estado.state) {
          return <Circle center={estado.coords} radius={object.cases * 1000} />;
        }
      });
    });
  }

  render() {
    return (
      <Map className="map" center={[-14.235, -51.9253]} zoom={5}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {this.renderCovid()}
        {/* 
        <HeatmapLayer
          fitBoundsOnLoad
          fitBoundsOnUpdate
          points={getHeatPoints()}
          longitudeExtractor={(m) => m[1]}
          latitudeExtractor={(m) => m[0]}
          gradient={{
            0.1: '#89BDE0',
            0.2: '#96E3E6',
            0.4: '#82CEB6',
            0.6: '#FAF3A5',
            0.8: '#F5D98B',
            1.0: '#DE9A96',
          }}
          intensityExtractor={(m) => {
            parseFloat(m[2]);
          }}
          radius={60}
          blur={60}
        /> */}

        <ImageOverlay
          url={require('../cbimage.png')}
          bounds={[
            [10.3, -75],
            [-34.4, -29.84],
          ]}
          opacity={0.6}
        />
      </Map>
    );
  }
}

export default MyMap;
