import React from 'react';
import './App.css';
import MyMap from './components/MyMap.js';

class App extends React.Component {
  render() {
    return <MyMap />;
  }
}

export default App;
